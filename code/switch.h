/*
 * SUEVS Motor Driver 2016
 * switch.h
 * Switch Header File
 *
 * Adrian Wheeldon
 * June 2016
 */
#ifndef SWITCH_H
#define SWITCH_H

void init_power_switch(void);
int power_switch_is_pressed(void);

#endif /* SWITCH_H */
