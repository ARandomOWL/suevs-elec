/*
 * SUEVS Motor Driver 2016
 * timer.c
 * Useful Timer Functions
 *
 * Adrian Wheeldon
 * June 2016
 */
#include <avr/io.h>
#include "timer.h"

uint8_t timer0_prescaler_bits(unsigned int prescaler)
{
	switch (prescaler) {
	case 1:
		return _BV(CS00);
		break;
	case 8:
		return _BV(CS01);
		break;
	case 64:
		return _BV(CS01) | _BV(CS00);
		break;
	case 256:
		return _BV(CS02);
		break;
	case 1024:
		return _BV(CS02) | _BV(CS00);
		break;
	default:
		return _BV(CS00);
		break;
	}
}

uint8_t timer1_prescaler_bits(unsigned int prescaler)
{
	switch (prescaler) {
	case 1:
		return _BV(CS10);
		break;
	case 8:
		return _BV(CS11);
		break;
	case 64:
		return _BV(CS11) | _BV(CS10);
		break;
	case 256:
		return _BV(CS12);
		break;
	case 1024:
		return _BV(CS12) | _BV(CS10);
		break;
	default:
		return _BV(CS10);
		break;
	}
}

uint8_t timer2_prescaler_bits(unsigned int prescaler)
{
	switch (prescaler) {
	case 1:
		return _BV(CS20);
		break;
	case 8:
		return _BV(CS21);
		break;
	case 32:
		return _BV(CS21) | _BV(CS20);
		break;
	case 64:
		return _BV(CS22);
		break;
	case 128:
		return _BV(CS22) | _BV(CS20);
		break;
	case 256:
		return _BV(CS22) | _BV(CS21);
		break;
	case 1024:
		return _BV(CS22) | _BV(CS21) | _BV(CS20);
		break;
	default:
		return _BV(CS20);
		break;
	}
}


