/*
 * SUEVS Motor Driver 2016
 * main.c
 * Main C File
 *
 * This project follows the Linux kernel coding style.
 * Please see 01coding_style.txt for details.
 *
 * Adrian Wheeldon
 * May 2016
 */
#include "hardware.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "switch.h"
#include "led.h"
#include "screen.h"
#include "throttle.h"
#include "motor.h"
#include "speed.h"

static void fatal_error(void);

float display_speed = 0;

int main(void)
{
	if (init_led())
		;	/* LED initialization failed */
	led_set();

	init_power_switch();

	if (init_screen())
		;	/* Screen initialization failed */

	if (init_throttle())
		;	/* Throttle initialization failed */

	if (init_speed_sensor())
		;	/* Speed sensor initialization failed */

	if (init_motor())
		;	/* Motor initialization failed */

	/*
	 * Wait for power switch to be pressed and released before vehicle can
	 * be operated.
	 */
//	while (!power_switch_is_pressed());
//	_delay_ms(50.0);	/* crude debouncing */
//	while (power_switch_is_pressed());

	/*
	 * SYSTEM IS NOW LIVE!
	 */

	sei();	/* Global interrupt enable */

	for (;;) {	/* Main program loop */
		int i = SCREEN_UPDATE_DELAY / PWM_UPDATE_DELAY;
		for (; i > 0; --i) {
			motor_set_pwm(throttle_get_requested_pwm());
			_delay_ms(PWM_UPDATE_DELAY);
		}
		if (speed_get_current() < 1.0)
			screen_update(0.0);
		else
			screen_update(display_speed);
	}
	return 0;
}

static void fatal_error(void)
{
	for (;;) {
		led_toggle();
		_delay_ms(250.0);
	}
}

void update_speed(void)
{
	float current_speed = speed_get_current();
	display_speed = SPEED_DISPLAY_MPH ? speed_convert_to_mph(current_speed)
		: current_speed;
}

ISR(SPEED_TIMER_CAPTURE_HANDLER)
{
	speed_clear_counter();
	update_speed();
	//motor_set_speed(current_speed, throttle_get_requested_speed());
}
