/*
 * SUEVS Motor Driver 2016
 * led.c
 * LED Driver
 *
 * Adrian Wheeldon
 * May 2016
 */
#include "led.h"
#include "hardware.h"

int init_led(void)
{
	LED_DIR |= LED_BIT;
	return 0;
}

int led_set(void)
{
	LED_PORT |= LED_BIT;
	return 0;
}

int led_clear(void)
{
	LED_PORT &= ~LED_BIT;
	return 0;
}

int led_toggle(void)
{
	LED_PIN |= LED_BIT;
	return 0;
}
