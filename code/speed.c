/*
 * SUEVS Motor Driver 2016
 * speed.c
 * Speed Sensor Driver
 *
 * Adrian Wheeldon
 * May 2016
 */
#include <limits.h> /* For UINT_MAX */
#include <avr/interrupt.h>
#include "hardware.h"
#include "speed.h"
#include "timer.h"

uint8_t overflow_count;

int init_speed_sensor(void)
{
	SPEED_TIMER_TCNTx = 0;
	overflow_count = 0;
	SPEED_INPUT_DIR &= ~SPEED_INPUT_BIT;	/* Set ICPx as input */

	/* Enable overflow interrupt */
	SPEED_TIMER_TIMSKx |= _BV(SPEED_TIMER_TOIEx);
	/* Enable input capture interrupt */
	SPEED_TIMER_TIMSKx |= _BV(SPEED_TIMER_ICIEx);

	if (SPEED_INPUT_CAPTURE_RISING)
		SPEED_TIMER_TCCRxB |= _BV(SPEED_TIMER_ICESx);
	else
		SPEED_TIMER_TCCRxB &= ~_BV(SPEED_TIMER_ICESx);

	/* Enable timer with prescaler */
	SPEED_TIMER_TCCRxB |= timer1_prescaler_bits(SPEED_TIMER_PRESCALER);
	return 0;
}

/*
 * Returns current speed in m/s.
 */
float speed_get_current(void)
{
	float time_between_pulses =
		((SPEED_TIMER_RESOLUTION * overflow_count) + SPEED_TIMER_ICRx)
		* SPEED_TIMER_PERIOD;
	return SPEED_WHEEL_CIRCUMFERENCE / time_between_pulses;
}

float speed_get_current_mph(void)
{
	return speed_convert_to_mph(speed_get_current());
}

float speed_convert_to_mph(float speed_in_metre_per_sec)
{
	return speed_in_metre_per_sec * 2.237;
}

ISR(SPEED_TIMER_OVERFLOW_HANDLER)
{
	if (overflow_count != UINT8_MAX)
		overflow_count += 1;
}

/* Must be called by the SPEED_TIMER_CAPTURE_HANDLER ISR */
void speed_clear_counter(void)
{
	SPEED_TIMER_TCNTx = 0;
	overflow_count = 0;
}
