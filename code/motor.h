/*
 * SUEVS Motor Driver 2016
 * motor.h
 * Motor Driver Header File
 *
 * Adrian Wheeldon
 * May 2016
 */
#ifndef MOTOR_H
#define MOTOR_H

#define MOTOR_PID_FREQ		(F_CLKIO / (510.0 * MOTOR_PID_PRESCALER))
#define MOTOR_PID_PERIOD	(1.0 / MOTOR_PID_FREQ)
#define MOTOR_PWM_FREQ		(F_CLKIO / (510.0 * MOTOR_PWM_PRESCALER))
#define MOTOR_PWM_PERIOD	(1.0 / MOTOR_PWM_FREQ)
#define MOTOR_DEADTIME_PWM_RAW	((MOTOR_DEADTIME / (1.0 / MOTOR_PWM_FREQ)) \
				* MOTOR_PWM_RESOLUTION)
#define MOTOR_DEADTIME_PWM	((MOTOR_IS_OUTPUT_INVERTED ? -1 : 1) \
				* MOTOR_DEADTIME_PWM_RAW)

#define MOTOR_OCR_LIMIT_EXTREME(__requested1) \
	max(0, min(MOTOR_PWM_RESOLUTION - 1, __requested1))
#define MOTOR_OCR_LIMIT(__requested2) \
	((MOTOR_OCR_LIMIT_EXTREME(__requested2) <= MOTOR_DEADTIME_PWM_RAW) ? \
	0 : \
	(MOTOR_OCR_LIMIT_EXTREME(__requested2) >= \
		MOTOR_PWM_RESOLUTION - 1 - MOTOR_DEADTIME_PWM_RAW) ? \
	MOTOR_PWM_RESOLUTION : \
	(__requested2))

int init_motor(void);
void motor_set_pwm(int pwm_value);
int motor_set_speed(float current_speed, float new_speed);

#endif /* MOTOR_H */
