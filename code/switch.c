/*
 * SUEVS Motor Driver 2016
 * switch.c
 * Switch Driver
 *
 * Adrian Wheeldon
 * June 2016
 */
#include "switch.h"
#include "hardware.h"

void init_power_switch(void)
{
	SWITCH_DIR &= ~SWITCH_BIT;
}

int power_switch_is_pressed(void)
{
	return (SWITCH_ACTIVE_LOW ^ ((SWITCH_PIN & SWITCH_BIT) ? 1 : 0));
}
