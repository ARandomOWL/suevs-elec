/*
 * SUEVS Motor Driver 2016
 * motor.c
 * Motor Driver
 *
 * Adrian Wheeldon
 * May 2016
 */
#include <avr/io.h>
#include <inttypes.h>	/* For type conversion when passing SFRs as arguments */
#include "motor.h"
#include "timer.h"
#include "hardware.h"

static int motor_calculate_new_pwm(float current_speed, float requested_speed);
static void motor_init_pid(unsigned int prescaler);
static void motor_init_pwm(unsigned int prescaler, int is_output_inverted);
static void motor_set_pwm_waveform(volatile uint8_t *tccr, uint8_t com0,
				   uint8_t com1, int is_inverted);

/*
 * This function must ensure that the motor remains stationary throughout.
 */
int init_motor(void)
{
	motor_set_pwm(0);
	motor_init_pwm(MOTOR_PWM_PRESCALER, MOTOR_IS_OUTPUT_INVERTED);
	motor_init_pid(MOTOR_PID_PRESCALER);
	return 0;
}

/*
 * This function takes an integer argument to prevent wrap-around overflow.
 */
void motor_set_pwm(int pwm_value)
{
	if (MOTOR_ENABLE_HIGHSIDE)
		MOTOR_PWM_OCRxA = MOTOR_OCR_LIMIT(pwm_value - MOTOR_DEADTIME_PWM);

	if (MOTOR_ENABLE_LOWSIDE)
		MOTOR_PWM_OCRxB = MOTOR_OCR_LIMIT(pwm_value + MOTOR_DEADTIME_PWM);
}

int motor_set_speed(float current_speed, float new_speed)
{
	int motor_new_pwm_value =
		motor_calculate_new_pwm(current_speed, new_speed);
	motor_set_pwm(motor_new_pwm_value);
	return 0;
}

static int motor_calculate_new_pwm(float current_speed, float requested_speed)
{
	/*
	 * PID Loop
	 */
	float dt = MOTOR_PID_TCNTx * MOTOR_PID_PERIOD;
	MOTOR_PID_TCNTx = 0;	/* Reset timer used to find dt */
	static float error = 0;
	static float integral = 0;
	float derivative = -error;	/* derivative = -last_error */
	error = requested_speed - current_speed;
	integral += error;
	derivative += error;	/* derivative = -last_error + current_error */
	float p_term = MOTOR_PID_KP * error;
	float i_term = MOTOR_PID_KI * integral * dt;
	float d_term = MOTOR_PID_KD * derivative / dt;

	return p_term + i_term + d_term;
}

static void motor_init_pid(unsigned int prescaler)
{
	MOTOR_PID_TCNTx = 0;	/* Initialize counter */
	/* Enable timer with prescaler */
	MOTOR_PID_TCCRxB |= timer0_prescaler_bits(prescaler);
}

static void motor_init_pwm(unsigned int prescaler, int is_output_inverted)
{
	/*
	 * Phase Correct PWM mode.
	 * WGM[2:0] = 1 (0b001)
	 */
	MOTOR_PWM_TCCRxA |= _BV(MOTOR_PWM_WGMx0);
	MOTOR_PWM_TCCRxA &= ~_BV(MOTOR_PWM_WGMx1);
	MOTOR_PWM_TCCRxB &= ~_BV(MOTOR_PWM_WGMx2);

	/* Set PWM waveform generation modes and enable outputs */
	if (MOTOR_ENABLE_HIGHSIDE) {
		motor_set_pwm_waveform(&MOTOR_PWM_TCCRxA, MOTOR_PWM_COMxA0,
				       MOTOR_PWM_COMxA1, is_output_inverted);
		MOTOR_DIR |= MOTOR_HIGHSIDE_BIT;
	}

	if (MOTOR_ENABLE_LOWSIDE) {
		/*
		 * NB: Extra inversion required on is_output_inverted since
		 * lowside switch waveform must be inverse of highside.
		 */
		motor_set_pwm_waveform(&MOTOR_PWM_TCCRxA, MOTOR_PWM_COMxB0,
				       MOTOR_PWM_COMxB1, !is_output_inverted);
		MOTOR_DIR |= MOTOR_LOWSIDE_BIT;
	}

	MOTOR_PWM_TCNTx = 0;	/* Initialize counter */
	/* Enable timer with prescaler */
	MOTOR_PWM_TCCRxB |= timer2_prescaler_bits(prescaler);
}

static void motor_set_pwm_waveform(volatile uint8_t *tccr, uint8_t com0,
				   uint8_t com1, int is_inverted)
{
	/*
	 * For non-inverted waveform: COMxx0 = 0, COMxx1 = 1
	 * For inverted waveform: COMxx0 = 1, COMxx1 = 1.
	 */
	if (is_inverted) {
		*tccr |= _BV(com0);
		*tccr |= _BV(com1);
	} else {
		*tccr &= ~_BV(com0);
		*tccr |= _BV(com1);
	}
}
