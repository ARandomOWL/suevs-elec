/*
 * SUEVS Motor Driver 2016
 * hardware.h
 * Hardware Definitions Header File
 *
 * Adrian Wheeldon
 * May 2016
 */
#ifndef HARDWARE_H
#define HARDWARE_H

#include <avr/io.h>

#define F_CPU			12000000UL	/* 12 MHz */
#define CLKIO_PRESCALER		1
#define F_CLKIO			(F_CPU / CLKIO_PRESCALER)

#define PI			3.14159

#define min(__a,__b)	(((__a)<(__b))?(__a):(__b))
#define max(__a,__b)	(((__a)>(__b))?(__a):(__b))

/*
 * SCREEN_UPDATE_DELAY will be rounded down to nearest multiple of
 * PWM_UPDATE_DELAY.
 */
#define PWM_UPDATE_DELAY	50.0	/* measured in ms */
#define SCREEN_UPDATE_DELAY	250.0	/* measured in ms */

/*
 * Il Matto Pinout
 *
 * PA0 - Throttle Input
 *
 * PB7 - LED
 *
 * PD6 - Motor Lowside Drive / Speed Sensor Input
 * PD7 - Motor Highside Drive
 */

/*
 * Status LED
 */
#define LED_PORT		PORTB
#define LED_DIR			DDRB
#define LED_PIN			PINB
#define LED_BIT			_BV(PB7)

/*
 * Power Switch
 */
#define SWITCH_ACTIVE_LOW	1	/* 0 for active high */
#define SWITCH_DIR		DDRD
#define SWITCH_PIN		PIND
#define SWITCH_BIT		_BV(PD5)

/*
 * Motor
 *
 * The motor PWM operates in phase correct mode. The PWM frequency is given by:
 *	F_pwm = F_clk_i/o / (510 * prescaler).
 * See ATmega644p datasheet p.100 for more.
 */
#define MOTOR_IS_OUTPUT_INVERTED	1
#define MOTOR_ENABLE_LOWSIDE		0
#define MOTOR_ENABLE_HIGHSIDE		1
/*
 * Valid prescalers are:
 *	PID: 1, 8, 64, 256, 1024
 *	PWM: 1, 8, 32, 64, 128, 256, 1024 */
#define MOTOR_PID_PRESCALER	8.0
#define MOTOR_PWM_PRESCALER	1.0
#define MOTOR_DEADTIME		0.0	/* Measured in seconds */
/* PID constants */
#define MOTOR_PID_KP		1.0
#define MOTOR_PID_KI		0.0
#define MOTOR_PID_KD		0.0

/* PID timer */
#define MOTOR_PID_TCCRxA		TCCR0A
#define MOTOR_PID_TCCRxB		TCCR0B
#define MOTOR_PID_TCNTx			TCNT0
#define MOTOR_PID_WGMx0			WGM00
#define MOTOR_PID_WGMx1			WGM01
#define MOTOR_PID_WGMx2			WGM02
#define MOTOR_PID_CSx0			CS00
#define MOTOR_PID_CSx1			CS01
#define MOTOR_PID_CSx2			CS02
#define MOTOR_PID_COMxA0		COM0A0
#define MOTOR_PID_COMxA1		COM0A1
#define MOTOR_PID_COMxB0		COM0B0
#define MOTOR_PID_COMxB1		COM0B1
#define MOTOR_PID_OCRxA			OCR0A
#define MOTOR_PID_OCRxB			OCR0B
#define MOTOR_PID_RESOLUTION		(1 << 8)

/* PWM timer */
#define MOTOR_PWM_TCCRxA		TCCR2A
#define MOTOR_PWM_TCCRxB		TCCR2B
#define MOTOR_PWM_TCNTx			TCNT2
#define MOTOR_PWM_WGMx0			WGM20
#define MOTOR_PWM_WGMx1			WGM21
#define MOTOR_PWM_WGMx2			WGM22
#define MOTOR_PWM_CSx0			CS20
#define MOTOR_PWM_CSx1			CS21
#define MOTOR_PWM_CSx2			CS22
#define MOTOR_PWM_COMxA0		COM2A0
#define MOTOR_PWM_COMxA1		COM2A1
#define MOTOR_PWM_COMxB0		COM2B0
#define MOTOR_PWM_COMxB1		COM2B1
#define MOTOR_PWM_OCRxA			OCR2A
#define MOTOR_PWM_OCRxB			OCR2B
#define MOTOR_PWM_RESOLUTION		(1 << 8)

/* PWM pins - fixed dependent on timer used */
#define MOTOR_PORT			PORTD
#define MOTOR_DIR			DDRD
#define MOTOR_HIGHSIDE_BIT		_BV(PD7)
#define MOTOR_LOWSIDE_BIT		_BV(PD6)

/*
 * Throttle
 */
#define THROTTLE_PORT		PORTA
#define THROTTLE_DIR		DDRA
#define THROTTLE_DIDR		DIDR0
#define THROTTLE_BIT		_BV(PA0)
/* Valid options are: 2, 4, 8, 16, 32, 64, 128 */
#define THROTTLE_ADC_PRESCALER	128
#define THROTTLE_ADC_REF	3.3
#define THROTTLE_VOLTAGE_MIN	1.5
#define THROTTLE_VOLTAGE_MAX	1.8
#define	THROTTLE_MAX_REQUESTED_SPEED	20.0	/* measured in m/s */
#define THROTTLE_ADC_RESOLUTION	(UINT32_C(1) << 10)

/*
 * Speed Sensor
 */
#define SPEED_WHEEL_DIAMETER	0.5	/* Measured in metres */
#define SPEED_DISPLAY_MPH	1	/* 0 for m/s */
/* Valid prescalers are: 1, 8, 64, 256, 1024 */
#define SPEED_TIMER_PRESCALER	8.0
#define SPEED_INPUT_CAPTURE_RISING	0	/* 0 for falling edge */
/* Input Capture Pin - fixed */
#define SPEED_INPUT_PORT	PORTD
#define SPEED_INPUT_DIR		DDRD
#define SPEED_INPUT_BIT		_BV(PD6)

/* Timer */
#define SPEED_TIMER_TCNTx	TCNT1
#define SPEED_TIMER_ICRx	ICR1
#define SPEED_TIMER_TCCRxB	TCCR1B
#define SPEED_TIMER_TIMSKx	TIMSK1
#define SPEED_TIMER_ICESx	ICES1
#define SPEED_TIMER_ICIEx	ICIE1
#define SPEED_TIMER_TOIEx	TOIE1
#define SPEED_TIMER_RESOLUTION	(UINT32_C(1) << 16)

/* Interrupt Handlers */
#define SPEED_TIMER_OVERFLOW_HANDLER	TIMER1_OVF_vect
#define SPEED_TIMER_CAPTURE_HANDLER	TIMER1_CAPT_vect

/*
 * Screen
 */
#define controlport PORTB
#define controlddr DDRB
#define controlpin PINB
#define rstport PORTB
#define rstddr DDRB
#define rstpin PINB
#define dc PB0
#define cs PB2
#define rst PB1
#define ILI9341_TFTHEIGHT 240
#define ILI9341_TFTWIDTH 320

#endif /* HARDWARE_H */
