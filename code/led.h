/*
 * SUEVS Motor Driver 2016
 * led.h
 * LED Header File
 *
 * Adrian Wheeldon
 * May 2016
 */
#ifndef LED_H
#define LED_H

int init_led(void);
int led_set(void);
int led_clear(void);
int led_toggle(void);

#endif /* LED_H */
