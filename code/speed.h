/*
 * SUEVS Motor Driver 2016
 * speed.h
 * Speed Sensor Header File
 *
 * Adrian Wheeldon
 * May 2016
 */
#ifndef SPEED_H
#define SPEED_H

#define SPEED_WHEEL_CIRCUMFERENCE	(SPEED_WHEEL_DIAMETER * PI)
#define SPEED_TIMER_PERIOD		(1 / (F_CLKIO / SPEED_TIMER_PRESCALER))

int init_speed_sensor(void);
float speed_get_current(void);
float speed_get_current_mph(void);
float speed_convert_to_mph(float speed_in_metre_per_sec);
void speed_clear_counter(void);

#endif /* SPEED_H */
