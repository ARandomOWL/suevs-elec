/*
 * SUEVS Motor Driver 2016
 * throttle.h
 * Throttle Position Sensor Header File
 *
 * Adrian Wheeldon
 * May 2016
 */
#ifndef THROTTLE_H
#define THROTTLE_H

int init_throttle(void);
float throttle_get_requested_speed(void);
unsigned short throttle_get_requested_pwm(void);

#endif /* THROTTLE_H */
