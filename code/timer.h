/*
 * SUEVS Motor Driver 2016
 * timer.h
 * Useful Timer Functions Header File
 *
 * Adrian Wheeldon
 * June 2016
 */
#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

uint8_t timer0_prescaler_bits(unsigned int prescaler);
uint8_t timer1_prescaler_bits(unsigned int prescaler);
uint8_t timer2_prescaler_bits(unsigned int prescaler);

#endif /* TIMER_H */
