/*
 * SUEVS Motor Driver 2016
 * throttle.c
 * Throttle Position Sensor Driver
 *
 * Adrian Wheeldon
 * May 2016
 */
#include <avr/io.h>
#include <limits.h> /* For USHRT_MAX */
#include "throttle.h"
#include "hardware.h"

static void throttle_init_adc(unsigned short prescaler);

/*
 * Set up ADC for free running mode
 */
static void throttle_init_adc(unsigned short prescaler)
{
	unsigned short prescaler_bits;

	THROTTLE_DIR &= ~THROTTLE_BIT;
	/* Disable digital input buffer on corresponding pin */
	THROTTLE_DIDR |= THROTTLE_BIT;

	switch (prescaler) {
	case 2:
		prescaler_bits = 0;
		break;
	case 4:
		prescaler_bits = _BV(ADPS1);
		break;
	case 8:
		prescaler_bits = _BV(ADPS1) | _BV(ADPS0);
		break;
	case 16:
		prescaler_bits = _BV(ADPS2);
		break;
	case 32:
		prescaler_bits = _BV(ADPS2) | _BV(ADPS0);
		break;
	case 64:
		prescaler_bits = _BV(ADPS2) | _BV(ADPS1);
		break;
	case 128:
		prescaler_bits = _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
		break;
	default:
		prescaler_bits = 0;
		break;
	}
	ADCSRA |= prescaler_bits;

	ADCSRA |= _BV(ADATE);	/* Enable ADC auto-trigger */
	ADCSRA |= _BV(ADEN);	/* Enable ADC peripheral */
	ADCSRA |= _BV(ADSC);	/* Start first ADC conversion */
}

int init_throttle(void)
{
	throttle_init_adc(THROTTLE_ADC_PRESCALER);
	return 0;
}

float throttle_get_requested_speed(void)
{
	/* float promotion */
	float requested_pwm = throttle_get_requested_pwm();
	return (requested_pwm / USHRT_MAX) * THROTTLE_MAX_REQUESTED_SPEED;
}

unsigned short throttle_get_requested_pwm(void)
{
	/* ADC result is 10-bit. Discard 2 LSBs. */
	unsigned short adc_value = ADC & ~0x3;
	float adc_voltage = THROTTLE_ADC_REF
		* (adc_value / THROTTLE_ADC_RESOLUTION);
	float voltage_delta = THROTTLE_VOLTAGE_MAX - THROTTLE_VOLTAGE_MIN;
	float throttle_scaled = (adc_voltage - THROTTLE_VOLTAGE_MIN)
		* (USHRT_MAX / voltage_delta);
	throttle_scaled = max(throttle_scaled, 0);
	throttle_scaled = min(throttle_scaled, USHRT_MAX);
	return throttle_scaled;
}
